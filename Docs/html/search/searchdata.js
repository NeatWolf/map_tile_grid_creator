var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuv",
  1: "cdfghmpstv",
  2: "m",
  3: "cdfghmpstuv",
  4: "abcdefghilmorst",
  5: "_bcdefglmnos",
  6: "s",
  7: "ct",
  8: "ch",
  9: "gnps"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties"
};

