# __Map Tile Grid Creator Git__

![](logo_title.png)

Compatible Unity Versions : 
- 2019.4.0f1(LTS)
- 2018.4.22f1 with few changes in code.

An Unity extension for create and modify create 3D Regular Map with gameplay based on that.

![](preview.png)

## __Features__

- Level editor with prefab pallets
- Generic grid system (Hexagonal and Cube implemented)
- Runtime gameplay support
- Procedural modifications workflow
- Debug and utilities functions
- Undo/Redo operations

# __Dependencies Package Manager__

- Unity UI

## __Quick usage && Tutorials__

See the folder Assets/MapTileGridCreator/Tutorials in local or the [wiki](https://gitlab.com/m21-cerutti/map_tile_grid_creator/-/wikis/home).

## __Contact__

marc.cerutti@outlook.fr

---

MIT Licence Marc Cerutti 

See the file LICENCE for more informations.